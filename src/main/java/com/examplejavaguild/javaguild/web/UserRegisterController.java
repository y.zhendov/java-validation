package com.examplejavaguild.javaguild.web;

import com.examplejavaguild.javaguild.service.UserService;
import com.examplejavaguild.javaguild.web.binding.UserRegisterBinding;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@AllArgsConstructor
public class UserRegisterController {

    private final UserService userService;

    @GetMapping("/")
    public String index(Model model){
        if(!model.containsAttribute("registeredSuccessfully")){
            model.addAttribute("registeredSuccessfully",true);
            return "index";
        }

        if(!model.containsAttribute("correctInfoDetails")){
            model.addAttribute("correctInfoDetails",true);
            return "index";
        }
        if(!model.containsAttribute("userExists")){
            model.addAttribute("userExists",true);
            return "index";
        }

        return "index";
    }

    @PostMapping("/register")
    public String register(@Valid UserRegisterBinding userRegisterBinding,
                           BindingResult bindingResult,
                           RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("userRegisterBinding", userRegisterBinding);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.userRegisterBinding", bindingResult);
            redirectAttributes.addFlashAttribute("correctInfoDetails", false);
            return "redirect:/";
        }

        boolean register = this.userService.register(userRegisterBinding);

        if(!register){
            redirectAttributes.addFlashAttribute("userExists", false);

        }else {
            redirectAttributes.addFlashAttribute("registeredSuccessfully", false);
        }

        return "redirect:/";
    }

    @ModelAttribute
    public UserRegisterBinding userRegisterBinding(){
        return new UserRegisterBinding();
    }
}
