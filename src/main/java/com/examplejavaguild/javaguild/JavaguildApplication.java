package com.examplejavaguild.javaguild;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaguildApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaguildApplication.class, args);
    }

}
