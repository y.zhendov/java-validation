package com.examplejavaguild.javaguild.service.impl;

import com.examplejavaguild.javaguild.data.entity.User;
import com.examplejavaguild.javaguild.repository.UserRepository;
import com.examplejavaguild.javaguild.service.UserService;
import com.examplejavaguild.javaguild.web.binding.UserRegisterBinding;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private  final UserRepository userRepository;
    private  final ModelMapper modelMapper;

    @Override
    public boolean register(UserRegisterBinding userRegisterBinding) {
        try {
            User mappedUserRegister = modelMapper.map(userRegisterBinding, User.class);
            userRepository.saveAndFlush(mappedUserRegister);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
