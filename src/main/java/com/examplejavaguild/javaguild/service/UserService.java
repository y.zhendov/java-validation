package com.examplejavaguild.javaguild.service;

import com.examplejavaguild.javaguild.web.binding.UserRegisterBinding;

public interface UserService {
    boolean register(UserRegisterBinding userRegisterBinding);
}
